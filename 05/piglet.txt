digraph problem  { 

node [color=black, shape=circle]

N [label="N"] 

subgraph p1 
{ 
   label="whatever" 
   rank="same" 
   NN[label="N"]
} 

N -> NN [headlabel="1-θ", labeldistance=3, labelangle=-45, arrowhead=none]

subgraph p2
{ 
   label="whatever" 
   rank="same" 
   1 [label="P,«K|a»"]
   2 [label="P,«F|a»"]
   3 [label="P,«F»"]
} 

subgraph payoffs
{ 
    label="whatever" 
    rank="same" 
    4 [label="(25)", rank=sink, shape="plaintext"]
    5 [label="(0)", rank=sink, shape="plaintext"]
    6 [label="(1)", rank=sink, shape="plaintext"]
    7 [label="(5)", rank=sink, shape="plaintext"]
    8 [label="(1)", rank=sink, shape="plaintext"]
    9 [label="(5)", rank=sink, shape="plaintext"]
} 


N -> 1 [headlabel="θ", labeldistance=3, labelangle=45, arrowhead=none]
NN -> 2 [headlabel="κ", labeldistance=3, labelangle=-45, arrowhead=none]
NN -> 3 [headlabel="1-κ", labeldistance=3, labelangle=-45, arrowhead=none]
1 -> 2 [style=dashed, dir=none] 
2 -> 3 [style=invis]




1 -> 4 [headlabel="Hide", labeldistance=3, labelangle=45, arrowhead=none]
1 -> 5 [headlabel="Come", labeldistance=3, labelangle=-45, arrowhead=none]
2 -> 6 [headlabel="Hide", labeldistance=3, labelangle=45, arrowhead=none]
2 -> 7 [headlabel="Come", labeldistance=3, labelangle=-45, arrowhead=none]
3 -> 8 [headlabel="Hide", labeldistance=3, labelangle=45, arrowhead=none]
3 -> 9 [headlabel="Come", labeldistance=3, labelangle=-45, arrowhead=none]
} 
