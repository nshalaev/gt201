# gt201

Неделя 5

## Аудио-лекция

w05.mp3

## Материал к лекции

games_with_nature.pdf

## Задания к семинару

Два варианта по две задачи:

Problems-A.pdf  
Problems-B.pdf

Задачи сложные, поэтому дороже обычных. В группы объединяться можно, до 4-х человек, но  
при условии соблюдения режима самоизоляции (т.е. не встречайтесь лично, сотрудничайте   
удалённо!).  
Для выбора задания используйте монетку или игральный кубик (чёт/нечет). Биномиальное  
распределение покажет, полагались ли вы на случай, или выбирали по принципу "как у соседа"  
или "что бы полегче".